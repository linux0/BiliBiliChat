利用bilibli弹幕系统，实现实时聊天
每个视频都是一个独立的聊天室

BilibiliDanmu.py 弹幕发送
运行方法：
python BilibiliDanmu.py 链接地址(视频地址) 账号 密码
如：
python BilibiliDanmu.py http://www.bilibili.com/video/av5361186/ account password

BilibiliDanmuShow.py 弹幕显示
运行方法：
python BilibiliDanmuShow.py 链接地址(视频地址)
如：
python BilibiliDanmuShow.py http://www.bilibili.com/video/av5361186/

python版本 ：python 2.7 
需要安装 requests  link：https://pypi.python.org/pypi/requests
已知 Bug：
	1.（弹幕发送）发送中文弹幕乱码(Linux 系统(xubuntu )下测试没有问题。windows下测试出现问题)
	2.（弹幕发送）在等待时间内（间隔5秒才能发送一个弹幕）发送的内容不能再次发送，需要把内容修改一下才能发送。
	如：在等待时间内发送了“msg”
		等待时间结束后发送“msg”，会显示发送进入等待（实际并没有进入等待），只需把“msg”内容稍微更改，如改为“msg ”即可发送。
	3.（弹幕显示）获取弹幕时间较慢